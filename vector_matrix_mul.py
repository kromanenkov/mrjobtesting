#!/usr/bin/env python

import sys
from mrjob.job import MRJob
from mrjob.step import MRStep
from mrjob.protocol import ReprValueProtocol


class MatVecMul(MRJob):
    """ First join the vector to the matrix based on columns,
    (joinmap, joinred) then actually do the
    multiplication (multmap, multred) """

    OUTPUT_PROTOCOL = ReprValueProtocol

    def joinmap(self, key, line):
        vals = [float(v) for v in line.split()]
        if len(vals) == 2:
            # this is a piece of the vector
            yield (vals[0], (vals[1], ))
        else:
            # this is a piece of the matrix
            row = vals[0]
            for i in xrange(1, len(vals), 2):
                yield (vals[i], (row, vals[i+1]))

    def joinred(self, key, vals):
        vecval = 0.0  # setup default val to support sparse vectors
        matvals = []
        for val in vals:
            if len(val) == 1:
                vecval = val[0]
            else:
                matvals.append(val)

        for val in matvals:
            yield (val[0], val[1]*vecval)

    def sumred(self, key, vals):
        yield (None, (key, sum(vals)))

    def sortred(self, _, vals):
        vec = sorted(vals, key=lambda tup: tup[0])
        for v in vec:
            yield v

    def steps(self):
        return [MRStep(mapper=self.joinmap, reducer=self.joinred),
                MRStep(reducer=self.sumred),
                MRStep(reducer=self.sortred)]


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: {} <matr.txt> <vec.txt>. Exit now".format(sys.argv[0])
        exit(1)

    MatVecMul.run()
