#!/usr/bin/env python

import sys
from mrjob.job import MRJob
from mrjob.step import MRStep
import re


class TotalWordsCounter(MRJob):
    def mapper(self, _, line):
        yield "words", len(line.split())

    def reducer(self, key, values):
        yield "Total words count", sum(values)


class AverageWordNumberCounter(MRJob):
    def mapper(self, _, line):
        yield "words", len(line.split())

    def reducer(self, key, values):
        lines_num = 0
        words_num = 0
        for val in values:
            words_num += val
            lines_num += 1

        yield "Average words number per line", float(words_num) / lines_num


class WordFrequenceCounter(MRJob):
    def mapper(self, _, line):
        for word in line.split():
            yield re.sub('[.,\']', '', word), 1

    # Reducer for counting frequencies.
    def reducer1(self, key, values):
        occurence = 0
        for val in values:
            occurence += 1

        if occurence == 1:
            # Word is met only once in text.
            yield "uniq", 1
        else:
            # Word is met more that once in text.
            yield "repeated", occurence

    # Reducer for outputing unique and repeated words.
    def reducer2(self, key, values):
        if key == "uniq":
            yield "Unique words", sum(values)
        elif key == "repeated":
            yield "Repeated words", sum(values)

    def steps(self):
        return [MRStep(mapper=self.mapper, reducer=self.reducer1),
                MRStep(reducer=self.reducer2)]


class WordLengthCounter(MRJob):
    def mapper(self, _, line):
        for word in line.split():
            word = re.sub('[.,\']', '', word)
            yield "word", len(word)
            if len(word) == 3:
                yield "word3", 1
        if not line:
            return
        yield "max_length", max(line.split(), key=len)

    def reducer(self, key, values):
        if key == "word3":
            yield "Number of 3-letter words", sum(values)
        elif key == "max_length":
            yield "Word(s) of max length", max(values, key=len)
        elif key == "word":
            words_num = 0
            sum_length = 0.0
            for val in values:
                sum_length += val
                words_num += 1
            yield "Average letters number per word", sum_length / words_num


class LetterFrequenceCounter(MRJob):
    def mapper(self, _, line):
        for word in line.split():
            word = re.sub('[,\']', '', word)
            for char in word:
                yield char, 1

    def reducer1(self, key, values):
        if key == 'o':
            yield "Occurence of 'o' symbol", sum(values)
        if key == '.':
            yield "Number of sentences", sum(values)

        yield None, (key, sum(values))

    def reducer2(self, key, values):
        if key is None:
            yield "Most frequent letter(s)",\
                  max(values, key=lambda tup: tup[1])
        else:
            yield key, sum(values)

    def steps(self):
        return [MRStep(mapper=self.mapper, reducer=self.reducer1),
                MRStep(reducer=self.reducer2)]

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Please specify input file name. Exit now."
        exit(1)
    file_name = sys.argv[1]

    # Total words number.
    TotalWordsCounter(args=[file_name]).run()
    # Average word number per line.
    AverageWordNumberCounter(args=[file_name]).run()
    # Unique and repeated words numbers.
    WordFrequenceCounter(args=[file_name]).run()
    # Maximum legth word, average word length and number of words length 3.
    WordLengthCounter(args=[file_name]).run()
    # Most frequent letter, 'o' letter occurence and number of sentences.
    LetterFrequenceCounter(args=[file_name]).run()
