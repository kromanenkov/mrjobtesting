#!/usr/bin/env python

import numpy
import sys

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "Wrong arguments number. Usage: {} <matr.txt> <vec.txt>. Exit "\
              "now.".format(sys.argv[0])
        exit(1)

    matr_file = sys.argv[1]
    vec_file = sys.argv[2]
    # Dimensions.
    n, m = 0, 0

    with open(matr_file, 'r') as mfile:
        with open(vec_file, 'r') as vfile:
            for line in mfile:
                n += 1
            for line in vfile:
                m += 1

    matr = numpy.zeros((n, m))
    vec = numpy.zeros((m, 1))

    with open(matr_file, 'r') as mfile:
        with open(vec_file, 'r') as vfile:
            # Fill matrix.
            for line in mfile:
                vals = line.split()
                row = int(vals[0])
                for i in xrange(1, len(vals), 2):
                    matr[row, int(vals[i])] = float(vals[i+1])
            # Fill vector.
            for line in vfile:
                vals = line.split()
                vec[int(vals[0]), 0] = float(vals[1])

    numpy.set_printoptions(precision=20)
    res = matr.dot(vec)
    print res
