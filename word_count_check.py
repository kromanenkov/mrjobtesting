#!/usr/bin/env python

import collections
import re
import sys

lines = 0
# Total words number in text.
words_num = 0
words_list = []
# Number of sentences.
sent_num = 0

if len(sys.argv) < 2:
    print "Please, specify file name. Exit now."
    exit(1)

with open(sys.argv[1], 'r') as infile:
    for line in infile:
        lines += 1
        line = line.strip()
        words_num += len(line.split())
        for word in line.split():
            # Number of sentences is equal to '.' symbol occurence.
            if word.count('.'):
                sent_num += 1
            words_list.append(re.sub('[.,\']', '', word))

# Turn word list into a occurence dictionary.
counter = collections.Counter(words_list)
# Number of repeated words.
repeats_num = 0
# Number of unique words.
uniq_num = 0
# Total number of letters.
letters_num = 0
# Number of 3-letter words.
threegram_num = 0
# Letters frequencies.
letters_freq = collections.Counter()

for key in counter:
    for i in range(0, counter[key]):
        letters_freq += collections.Counter(key)
    if len(key) == 3:
        threegram_num += counter[key]
    letters_num += len(key) * counter[key]
    if counter[key] == 1:
        uniq_num += 1
    else:  # key > 1
        repeats_num += counter[key]

# Max letter's frequency.
max_freq = letters_freq.most_common(1)[0][1]
# Letters with max_freq.
most_freq_chars = {}

for char, freq in letters_freq.iteritems():
    if freq == max_freq:
        most_freq_chars[char] = freq

# Count frequency of 'o' symbol.
if not 'o' in letters_freq.keys():
    o_count = 0
else:
    o_count = letters_freq['o']

print "Total words number: {}".format(len(words_list))
print "Total lines number: {}".format(lines)
print "Average words number per line: {}".format(float(words_num) / lines)
print "Repeated words: {}".format(repeats_num)
print "Unique words: {}".format(uniq_num)
print "Word(s) of max length: {}".format(max(words_list, key=len))
print "Average letters number per word: {}".format(
    float(letters_num) / len(words_list))
print "Number of 3-letter words: {}".format(threegram_num)
print "Most frequent letter(s): {}".format(most_freq_chars)
print "Occurence of 'o' symbol is: {}".format(o_count)
print "Number of sentences: {}".format(sent_num)
